# encoding: utf-8



# import model
# import train_group
# import train_person
# import test_group
# import utils
#
# import os
# import torch
#
#
# hidden_dim_1 = 3000
# hidden_dim_2 = 500
#
# person_encoder = model.test_LSTM(512, hidden_dim_1, 3)
# person_encoder = person_encoder.cuda()
# checkpoint = torch.load('/home/ld/dataset/new_cad/save_model/p_model/Mon_Jan_08_07:52:44_2018_pmodel.tar')
# print ('Person_encoder acc:', checkpoint['all_acc'])
# person_encoder.load_state_dict(checkpoint['state_dict'])
#
# group_encoder = model.test_LSTM(3512, hidden_dim_2, 7)
# group_encoder = group_encoder.cuda()
# group_checkpoint = torch.load('/home/ld/dataset/new_cad/save_model/g_model/Thu_Jan_11_12:46:07_2018_gmodel.tar')
# group_encoder.load_state_dict(group_checkpoint['state_dict'])
# print ('Group_encoder acc:', group_checkpoint['all_acc'])
# utils.okb('Person & Group encoder have been loaded!')
#
# # print (person_encoder)
# # print (group_encoder)
#
# all_data = utils.data_split()
# print ('test_pkls_1:', all_data[0][1])
# test_group.testIters_Group(all_data[0][1], person_encoder=person_encoder, group_encoder=group_encoder)
# utils.okb('Test has been accomplished!')

import os
import cv2
import get_flowmap
import scipy.misc as sm
from PIL import Image
import numpy as np

def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step / 2:h:step, step / 2:w:step].reshape(2, -1)  # 以网格的形式选取二维图像上等间隔的点，这里间隔为16，reshape成2行的array
    fx, fy = flow[y, x].T  # 取选定网格点坐标对应的光流位移
    lines = np.vstack([x, y, x + fx, y + fy]).T.reshape(-1, 2, 2)  # 将初始点和变化的点堆叠成2*2的数组
    lines = np.int32(lines + 0.5)  # 忽略微笑的假偏移，整数化
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))  # 以初始点和终点划线表示光流运动
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)  # 在初始点（网格点处画圆点来表示初始点）
    return vis

im1_name = '/media/ld/dataset/volleyball/0/13286/13286.jpg'
print ('Demo for data/demo/{}'.format(im1_name))
# go into def demo(net, image_name):
im1_file = os.path.join('data/demo', im1_name)
im_o = cv2.imread(im1_file)

im_name = '/media/ld/dataset/volleyball/0/13286/13287.jpg'
print ('Demo for data/demo/{}'.format(im_name))
# go into def demo(net, image_name):
im_file = os.path.join('data/demo', im_name)
im_o_2 = cv2.imread(im_file)

# im_1 = im_o[:, :, (2, 1, 0)] # change channel
# im_2 = im_o[:, :, (2, 1, 0)]

flow = get_flowmap.get_flow(im_o, im_o_2)
cv2.imshow('123', draw_flow(im_o_2, flow))
print (flow[0])
# sm.toimage(flow[0], cmin=0, cmax=255).save('/media/ld/123.png')
# cv2.imwrite('/media/ld/123.png', flow[0])
#
# print (flow.shape)
# img = Image.fromarray(flow[0])
# print (img)
# img.save('/media/ld/123.png')
