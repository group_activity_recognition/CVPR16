# encoding: utf-8



import torch
from torch.autograd import Variable
torch.cuda.set_device(1)

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.image as mimg

import numpy as np
from PIL import Image

import os
import re
import pickle

from torchvision.models import resnet34

use_cuda = True


plabel_dic = {'waiting': 0, 'setting': 1, 'digging': 2, 'falling': 3, 'spiking': 4, 'blocking': 5,
              'jumping': 6, 'moving': 7, 'standing': 8}
glabel_dic = {'r_set': 0, 'r_spike': 1, 'r-pass': 2, 'r_winpoint': 3, 'l_winpoint': 4,
              'l-pass': 5, 'l-spike': 6, 'l_set': 7}
# if label in plabel_dic.keys():
#     numlabel = plabel_dic[label]
#     ohlabel = torch.LongTensor()



cnn_model = resnet34(pretrained=True).cuda() if use_cuda else resnet34(pretrained=True)

def person_anno(video_path):
    anno_path = video_path + 'annotations.txt'
    with open(anno_path, 'r') as f:
        annotations = f.readlines()
    list_dir = os.listdir(video_path)
    list_dir.remove('annotations.txt')

    anno_video_p = {}
    for clip in list_dir:
        clip_imgs = []
        for num in range(int(clip)-5, int(clip)+5):
            clip_path = video_path + clip + '/' + str(num) + '.jpg'
            clip_imgs.append(clip_path)
        for i, item in enumerate(annotations):
            anno_clip = annotations[i].split()
            if clip in item:
                break

        n_person = (len(anno_clip)-2) / 5
        anno_clip_p = {}
        for i in range(n_person):
            p_x = int(anno_clip[i*5 + 2])
            p_y = int(anno_clip[i*5 + 3])
            p_h = int(anno_clip[i*5 + 4])
            p_w = int(anno_clip[i*5 + 5])
            p_label = anno_clip[i*5 + 6]
            anno_clip_p['person_'+str(i)] = [p_x, p_y, p_h, p_w, p_label]

        anno_video_p['clip_'+clip] = anno_clip_p
    return anno_video_p

def cnnf_extract(video_path):

    # video_id = 0
    # training_imgv = {}
    annotations = person_anno(video_path)

    list_dir = os.listdir(video_path)
    list_dir.remove('annotations.txt')
    cnn_fetr_video = []


    for clip in list_dir:
        anno_clip = annotations['clip_'+clip]
        clip_imgs = []
        for num in range(int(clip)-5, int(clip)+5):
            clip_path = video_path + clip + '/' + str(num) + '.jpg'
            clip_imgs.append(clip_path)
        n_person = len(anno_clip.keys())


        input_var_img = torch.zeros(10 * n_person, 3, 224, 224)
        input_var_label = np.zeros((n_person, 1), dtype='int64')

        for person in range(n_person):
            p_x = int(anno_clip['person_' + str(person)][0])
            p_y = int(anno_clip['person_' + str(person)][1])
            p_h = int(anno_clip['person_' + str(person)][2])
            p_w = int(anno_clip['person_' + str(person)][3])
            p_label = anno_clip['person_' + str(person)][4]
            input_var_label[person][0] = int(plabel_dic[p_label])

            # print (p_x, p_y, p_x + p_w, p_y + p_h)
            # input_var_label[person] = torch.LongTensor(p_label)
            for i, frame in enumerate(clip_imgs):
                img = Image.open(frame)
                pimg_cut = np.array(img.crop((p_x, p_y, p_x+p_w, p_y+p_h)).resize((224, 224)))
                pimg_cut = torch.from_numpy(pimg_cut).float()

                input_var_img[person*10 + i, :, :, :] = pimg_cut

        # print (input_var_img.shape)
        # print (input_var_label)

        label = torch.LongTensor(input_var_label)
        real_label = Variable(label.view(-1)).cuda() if use_cuda else Variable(label.view(-1))

        # print (label)
        one_hot = torch.zeros(n_person, 9).scatter_(1, label, 1)
        input_var = Variable(input_var_img, volatile=True)
        input_var = input_var.cuda() if use_cuda else input_var

        _, cnn_fetr = cnn_model(input_var)
        # print (cnn_fetr.data.shape)

        cnn_fetr = cnn_fetr.view(10, n_person, -1)
        one_hot = Variable(one_hot).cuda() if use_cuda else Variable(one_hot)

        clip_data = (cnn_fetr, real_label, n_person)
        cnn_fetr_video.append(clip_data)
    # training_imgv[video_id] = cnn_fetr_video
    return cnn_fetr_video

def batch_newcad(split_dir, batch_size):
    with open(split_dir, 'r') as f:
        tmp = pickle.load(f)
        n_batch = len(tmp)/batch_size

    return n_batch, tmp


class Colors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'


def header(s): print(Colors.HEADER + s + Colors.ENDC)


def warn(s): print(Colors.WARNING + s + Colors.ENDC)


def failure(s): print(Colors.FAIL + s + Colors.ENDC)


def okb(s): print(Colors.OKBLUE + s + Colors.ENDC)


def ok(s): print(Colors.OKGREEN + s + Colors.ENDC)


def showPlot(points):
    plt.figure()
    fig, ax = plt.subplots()
    # this locator puts ticks at regular intervals
    loc = ticker.MultipleLocator(base=0.2)
    ax.yaxis.set_major_locator(loc)
    plt.plot(points)
    plt.savefig('result.jpg')


def person_pooling(self, p_sfetr, p_tfetr):
    merged_tens = torch.cat((p_sfetr, p_tfetr), -1)


def data_split():
    data = []
    for i in range(3):
        all_pkls = os.listdir('/media/ld/dataset/new_cad/new_scratch/')
        test_num = [[0, 1, 6, 11, 12, 18, 19, 20, 25, 26, 29], [2, 4, 9, 10, 14, 15, 16, 17, 23, 24, 30],
         [3, 5, 7, 8, 13, 21, 22, 27, 28, 31]]
        test_pkls = ['split0_vid' + str(j) + '_im.pkl' for j in test_num[i]]
        for j in range(len(test_pkls)):
            all_pkls.remove(test_pkls[j])
        training_pkls = all_pkls

        data.append([training_pkls, test_pkls])
    return data

def get_num(npy_name):
    s_data = npy_name.split('_')
    # print (s_data)
    n_split = int(re.findall('[0-9]', s_data[0])[0])
    # n_video = 10 * int(re.findall('[0-99]', s_data[2])[0]) + int(re.findall('[0-99]', s_data[2])[1])
    n_video = int(re.findall('[0-99]', s_data[1])[0])

    fall_num = len(re.findall('[0-99]', s_data[2]))
    n_frame = 0
    for m, item in enumerate(re.findall('[0-99]', s_data[2])):
        n_frame += 10 ** (fall_num - m - 1) * int(item)
    n_person = int(re.findall('[0-9]', s_data[3])[0])

    return n_video, n_frame, n_person

def set_pre_labels(label_map):
    real_pre, pre_index = torch.mode(label_map)
    return real_pre

def confusion_matrix(prediction, target):
    result = torch.zeros(3, 4)
    tmp = 0
    for i, item in enumerate(prediction):
        tmp += (item == target[i])
        pre_id = item

        real_id = target[i]

        if real_id == 0:
            result[0][0] += 1
            if pre_id == 0:
                result[0][1] += 1
            if pre_id == 1:
                result[0][2] += 1
            if pre_id == 2:
                result[0][3] += 1
        if real_id == 1:
            result[1][0] += 1
            if pre_id == 0:
                result[1][1] += 1
            if pre_id == 1:
                result[1][2] += 1
            if pre_id == 2:
                result[1][3] += 1
        if real_id == 2:
            result[2][0] += 1
            if pre_id == 0:
                result[2][1] += 1
            if pre_id == 1:
                result[2][2] += 1
            if pre_id == 2:
                result[2][3] += 1

    return tmp, result
# class VideoAnnotation(object):
#     def __init__(self, clabel):
#         """
#         Arguments:
#         - `clabel`:
#         - `tid`:
#         - `bbox`:
#         - `actions`:
#         - `poses`:
#         - `interactions`:
#         - `ilines`:
#         """
#         self.clabel = clabel
#         self.tid = []
#         self.bbox = []
#         self.actions = []
#         self.poses = []
#         self.interactions = []
#         self.ilines = []
#
#     def __len__(self):
#         return len(self.bbox)
#
# with open('/media/ld/dataset/new_cad/new_anno.pkl', 'r') as f:
#     all_anno = pickle.load(f)

def get_bbox(seq_id, n_frame, n_person):
    x, y, w, h = all_anno[seq_id]['vidanno'][n_frame].bbox[n_person]
    return x, y

def move_encoder(seq_id, frame_1, frame_2, frame_3, n_person, alpha=0.8, beta=0.2, move_code=1):
    # move_code = [move left, x stand still, move right, move up, y stand still, move down]
    print (move_code)
    x_1, y_1 = get_bbox(seq_id, frame_1, n_person)
    x_2, y_2 = get_bbox(seq_id, frame_2, n_person)
    x_3, y_3 = get_bbox(seq_id, frame_3, n_person)

    x_threshold = 5
    y_threshold = 1

    delta_x = alpha * (x_2 - x_1) + beta * (x_3 - x_1)
    delta_y = alpha * (y_2 - y_1) + beta * (y_3 - y_1)
    if (abs(delta_x) < x_threshold) or (abs(delta_y) < y_threshold):
        move_code = 0
    else:
        move_code = 1
    return move_code







