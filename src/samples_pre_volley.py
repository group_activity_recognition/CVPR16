# coding: utf-8
import matplotlib.pyplot as plt

import datetime
import numpy as np

import skimage.io
import skimage.transform

import tensorflow as tf
import tensorflow.contrib.slim as slim

import inception
import pickle

from detnet import det_net, det_net_loss
import nnutil
import volleyball
from volleyball import *

import os

# config


class Config(object):

    def __init__(self):

        # shared
        self.image_size = 720, 1280
        self.out_size = 87, 157
        self.batch_size = 8
        self.num_boxes = 12
        self.epsilon = 1e-5
        self.features_multiscale_names = ['Mixed_5d', 'Mixed_6e']
        self.train_inception = True

        # DetNet
        self.build_detnet = True
        self.num_resnet_blocks = 1
        self.num_resnet_features = 512
        self.reg_loss_weight = 10.0
        self.nms_kind = 'greedy'

        # ActNet
        self.crop_size = 5, 5
        self.num_features_boxes = 2048
        self.num_actions = 9
        self.num_activities = 8
        self.actions_loss_weight = 0.5
        self.actions_weights = [[1., 1., 2., 3., 1., 2., 2., 0.2, 1.]]

        self.attention_kind = 'maxpool'
        self.attention_tau = 10.0
        self.attention_num_hidden = 512

        # sequence
        self.num_features_hidden = 1024
        self.num_frames = 10
        self.num_before = 5
        self.num_after = 4

        # training parameters
        self.train_num_steps = 5000
        self.train_random_seed = 0
        self.train_learning_rate = 1e-5
        self.train_dropout_prob = 0.8
        self.train_save_every_steps = 500
        self.train_max_to_keep = 8


c = Config()
# NOTE: you have to fill this
c.models_path = '<path-to-models>'
c.data_path = '/media/ld/dataset/volleyball/'
# reading images of a certain resolution
c.images_path = '/%dp' % c.image_size[0]

# reading the dataset
train = volley_read_dataset(c.data_path, volleyball.TRAIN_SEQS + volleyball.VAL_SEQS)
train_frames = volley_all_frames(train)
test = volley_read_dataset(c.data_path, volleyball.TEST_SEQS)
test_frames = volley_all_frames(test)
# all_anns = {**train, **test}
all_anns = {train, test}
all_frames = train_frames + test_frames

# these you can get in the repository
# The .pkl file is dumped by python3, so we should load it again using
# python3,and dump it again(protocol=2)
src_image_size = pickle.load(open(c.data_path + '/src_image_size.pkl', 'rb'))
all_tracks = pickle.load(open(c.data_path + '/tracks_normalized.pkl', 'rb'))


def load_samples_full(anns, tracks, images_path, frames, num_boxes=12):
    # NOTE: this assumes we got the same # of boxes for this batch
    images, reg_masks, seg_masks = [], [], []
    boxes, boxes_idx, activities, actions = [], [], [], []

    for i, (sid, src_fid) in enumerate(frames):
        # TODO: change me for sequence
        fid = src_fid
        images.append(skimage.io.imread(images_path + '/%d/%d/%d.jpg' %
                                        (sid, src_fid, fid)))
        image_size = images[-1].shape[:2]

        yx = np.mgrid[0:image_size[0], 0:image_size[1]]

        bbs = np.copy(tracks[(sid, src_fid)][fid])
        bbs[:, [0, 2]] = bbs[:, [0, 2]].astype(np.float32) * image_size[0]
        bbs[:, [1, 3]] = bbs[:, [1, 3]].astype(np.float32) * image_size[1]
        bbs = bbs.astype(np.int32)

        reg_mask = np.zeros(image_size + (4,), dtype=np.float32)
        for y0, x0, y1, x1 in bbs:
            reg_mask[y0:y1, x0:x1, 0] = (
                yx[0][y0:y1, x0:x1] - y0) / image_size[0]
            reg_mask[y0:y1, x0:x1, 1] = (
                yx[1][y0:y1, x0:x1] - x0) / image_size[1]
            reg_mask[y0:y1, x0:x1, 2] = (
                y1 - yx[0][y0:y1, x0:x1]) / image_size[0]
            reg_mask[y0:y1, x0:x1, 3] = (
                x1 - yx[1][y0:y1, x0:x1]) / image_size[1]
        seg_mask = np.any(reg_mask, axis=2).astype(np.uint8)
        reg_masks.append(reg_mask)
        seg_masks.append(seg_mask)

        boxes.append(tracks[(sid, src_fid)][fid])
        actions.append(anns[sid][src_fid]['actions'])
        if len(boxes[-1]) != num_boxes:
            boxes[-1] = np.vstack([boxes[-1],
                                   boxes[-1][:num_boxes - len(boxes[-1])]])
            actions[-1] = actions[-1] + \
                actions[-1][:num_boxes - len(actions[-1])]
        boxes_idx.append(i * np.ones(num_boxes, dtype=np.int32))
        activities.append(anns[sid][src_fid]['group_activity'])

    images = np.stack(images)
    activities = np.array(activities, dtype=np.int32)
    boxes = np.vstack(boxes).reshape([-1, num_boxes, 4])
    boxes_idx = np.hstack(boxes_idx).reshape([-1, num_boxes])
    actions = np.hstack(actions).reshape([-1, num_boxes])
    reg_masks = np.stack(reg_masks)
    seg_masks = np.stack(seg_masks)

    return images, activities, boxes, boxes_idx, actions, reg_masks, seg_masks
