# encoding: utf-8


from __future__ import unicode_literals, print_function, division
import torch
import torchvision
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F


import time
import random
import math
import os
import skimage.io as io

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from collections import defaultdict

import numpy as np
import data_processing
import model
import utils
import get_flowmap


use_cuda = True


all_pkls = os.listdir('/media/ld/dataset/new_cad/new_scratch/')
test_pkls = [all_pkls[1]]


base_path = '/media/ld/dataset/new_cad/new_scratch/'

def test_person(input_variable, target_variable, encoder, batch_size, criterion, n_steps=9):
    encoder_hidden = encoder.initHidden(batch_size)
    encoder_outputs, encoder_hidden, p_tfetr = encoder(input_variable, encoder_hidden)

    pres = encoder_outputs.view(n_steps, 3)
    topv, topi = encoder_outputs.data.topk(1)

    # tmp = 0
    # for i, item in enumerate(topi):
    #     tmp += (item == target_variable.data[i]).cpu().numpy()[0][0]

    result = torch.zeros(3, 4)
    tmp = 0
    for i, item in enumerate(topi):
        tmp += (item == target_variable.data[i]).cpu().numpy()[0][0]
        pre_id = item.cpu().numpy()[0][0]

        real_id = target_variable.cpu().data[i]

        if real_id == 0:
            result[0][0] += 1
            if pre_id == 0:
                result[0][1] += 1
            if pre_id == 1:
                result[0][2] += 1
            if pre_id == 2:
                result[0][3] += 1
        if real_id == 1:
            result[1][0] += 1
            if pre_id == 0:
                result[1][1] += 1
            if pre_id == 1:
                result[1][2] += 1
            if pre_id == 2:
                result[1][3] += 1
        if real_id == 2:
            result[2][0] += 1
            if pre_id == 0:
                result[2][1] += 1
            if pre_id == 1:
                result[2][2] += 1
            if pre_id == 2:
                result[2][3] += 1

    loss = criterion(pres, target_variable)

    return loss, tmp, result, p_tfetr, topi


def testIters_p(model_name, test_pkls, encoder, n_iters=1, print_every=10, plot_every=10, batch_size=1, n_steps=9):
    start = time.time()
    criterion = nn.CrossEntropyLoss()

    all_loss = 0
    all_acc = 0

    all_result = torch.zeros(3, 4)
    real_allresult = torch.zeros(3, 4)
    padding = torch.zeros(n_steps, 1, 227, 227)
    for iter in range(n_iters):
        all_batch = 0
        for pkl in test_pkls:
            n_batch, tmp = utils.batch_newcad(base_path + pkl, batch_size)
            all_batch += n_batch
            utils.ok('Current pkl: %s,   Containing batches: %d' % (pkl, n_batch))

            target_dict = {}
            pre_dict = {}

            for idx in range(n_batch):
                batch_data = tmp[idx: idx + batch_size][0]
                n_person = len(batch_data)
                batch_loss = 0
                batch_correct = 0
                # print ('load batch_data')
                # print (n_person)

                for person in range(n_person):

                    raw_x = torch.zeros(n_steps, 3, 227, 227)
                    raw_x_flow = torch.zeros(n_steps, 2, 227, 227)

                    n_video, start_frame, person_id = utils.get_num(batch_data[person][0][0][33:])
                    if 'person%d' % person_id not in target_dict.keys():
                        target_dict['person%d' % person_id] = (torch.zeros(n_steps)+4).int()

                    if 'person%d' % person_id not in pre_dict.keys():
                        pre_dict['person%d' % person_id] = (torch.zeros(n_steps)+4).int()

                    for frame in range(n_steps):

                        npy = io.imread('/media' + batch_data[person][0][frame][5:] + '.jpg')
                        # print ('npy', npy)
                        npy_f = npy.reshape(-1, 227, 227).astype('float32')
                        # print ('npy_F', npy_f)
                        raw_x[frame] = torch.from_numpy(npy_f)

                    y = torch.from_numpy(np.array(batch_data[person][1])).long()
                    # print (y)
                    # print ('predict', target_dict.keys())
                    target_dict['person%d' % person_id][-9:] = y
                    # if 'person3' in target_dict.keys():
                        # print (target_dict['person3'])
                    target_dict['person%d' % person_id] = torch.cat((target_dict['person%d' % person_id], (torch.zeros(1)+4).int()))

                    raw_x = Variable(raw_x.cuda()) if use_cuda else Variable(raw_x)
                    y = Variable(y.cuda()) if use_cuda else Variable(y)

                    x = model.cnn_model(raw_x)
                    x = x.view(n_steps, batch_size, -1)

                    try:
                        cb_loss, cb_tmp, cb_result, cb_ptf, topi = test_person(x, y, encoder, batch_size, criterion)
                        batch_loss += cb_loss
                        batch_correct += cb_tmp

                        pre_dict['person%d' % person_id][-9:] = topi
                        pre_dict['person%d' % person_id] = torch.cat(
                            (pre_dict['person%d' % person_id], (torch.zeros(1) + 4).int()))

                        all_result += cb_result
                        # set_result[idx:idx+n_steps, idx, person_id] = topi
                    except RuntimeError as e:
                        print(e.message)

                final_loss = batch_loss.data.cpu().numpy()[0] / batch_size
                final_acc = (100 * batch_correct / (n_person * n_steps)) / batch_size
                all_loss += final_loss
                all_acc += final_acc

                print('Test_P: %s %d/%d----> batch_loss: %.4f----> batch_acc: %.2f %%'
                      % (pkl, idx, n_batch, final_loss, final_acc))

            # for i in range(10):
            #     real_pre[:, i] = utils.set_pre_labels(set_result[:, :, i])

            # print (target_dict)
            # print (pre_dict)
            result = torch.zeros(3, 4)
            for key in pre_dict.keys():
                result += utils.confusion_matrix(pre_dict[key], target_dict[key])[1]

            # utils.ok('Result * 5')
            # print (result)
            real_allresult += result


    torch.save({'n_iters': n_iters,
                'all_batch': all_batch,
                'all_loss': all_loss / (all_batch * n_iters),
                'all_acc': all_acc / (all_batch * n_iters),
                'all_result': all_result.int() / n_iters,
                'real_allresult': real_allresult.int()},
               '/media/ld/dataset/new_cad/save_result/%s_presult.tar' % model_name)
def ptest_main(saved_model):
    hidden_dim_1 = 3000
    encoder = model.test_LSTM(4096, hidden_dim_1, 3).cuda()
    all_data = utils.data_split()
    checkpoint = torch.load(saved_model)
    encoder.load_state_dict(checkpoint['state_dict'])
    model_name = saved_model[45:-11]
    testIters_p(model_name, all_data[0][1], encoder, n_iters=1)

# ptest_main('/media/ld/dataset/new_cad/save_model/p_model/Mon_Feb_05_16:27:08_2018_0_pmodel.tar')
