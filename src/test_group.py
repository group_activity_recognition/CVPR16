# encoding: utf-8

from __future__ import unicode_literals, print_function, division
import torch
import torchvision
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim


import time
import random
import math
import os

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from collections import defaultdict

import numpy as np
import data_processing
import model
import utils
from test_person import test_person
import skimage.io as io
# if optical_flow map involved, use this
# import get_flowmap

# torch.cuda.is_available()
use_cuda = True

def test_group(input_variable, target_variable, encoder, encoder_hidden,
                 criterion, n_steps=9):

    # print(input_variable.size())
    encoder_outputs, encoder_hidden, _ = encoder(input_variable, encoder_hidden)
    pres = encoder_outputs.view(n_steps, 7)
    # print ('pres', pres)

    topv, topi = encoder_outputs.data.topk(1)
    tmp = 0
    for i, item in enumerate(topi):
        tmp += (item == target_variable.data[i]).cpu().numpy()[0][0]

    loss = criterion(pres, target_variable)
    # print ('loss:', loss)
    # p_tfetr shape(seq_length, batch_size, hidden_dim)
    return loss, tmp

def testIters_Group(model_name, test_pkls, person_encoder, group_encoder, n_iters=1, print_every=10, plot_every=10, batch_size=1, n_steps=9):
    start = time.time()
    plot_losses = []
    print_loss_total = 0
    plot_loss_total = 0

    # TODO: Initialize the h&c by values stored in loaded model?
    person_encoder_hidden = person_encoder.initHidden(batch_size)
    group_encoder_hidden = group_encoder.initHidden(batch_size)
    # print ('init hidden:', group_encoder_hidden)
    criterion = nn.CrossEntropyLoss()

    # training_pkls = os.listdir('/home/ld/dataset/new_cad/new_scratch/')
    base_path = '/media/ld/dataset/new_cad/new_scratch/'
    # training_pkls = ['/home/ld/dataset/new_cad/new_scratch/split0_vid0_im.pkl']

    all_acc = 0.00
    all_loss = 0.00
    padding = torch.zeros(n_steps, 1, 227, 227)

    for n_iter in range(n_iters):
        utils.okb('Current iter: %d' % n_iter)
        all_batch = 0
        for pkl in test_pkls:
            n_batch, tmp = utils.batch_newcad(base_path + pkl, batch_size)
            utils.ok('Current pkl: %s,   Containing batches: %d' % (pkl, n_batch))
            all_batch += n_batch

            for idx in range(n_batch):
                batch_data = tmp[idx: idx + batch_size][0]
                n_person = len(batch_data)
                batch_loss = 0
                batch_correct = 0
                # batch_ft = os.path.
                # if batch_ft

                batch_ft = Variable(torch.zeros(n_steps, batch_size, 7096+4096, n_person).cuda()) if use_cuda else Variable(torch.zeros(n_steps, batch_size, 3512, n_person))

                z = torch.from_numpy(np.array(batch_data[0][2])).long()
                z = Variable(z.cuda()) if use_cuda else Variable(z)

                for person in range(n_person):

                    raw_x = torch.zeros(n_steps, 3, 227, 227)
                    raw_x_flow = torch.zeros(n_steps, 2, 227, 227)

                    n_video, start_frame, person_id = utils.get_num(batch_data[person][0][0][33:])

                    for frame in range(n_steps):

                        npy = io.imread('/media' + batch_data[person][0][frame][5:] + '.jpg')
                        # print ('npy', npy)
                        npy_f = npy.reshape(-1, 227, 227).astype('float32')
                        # print ('npy_F', npy_f)
                        raw_x[frame] = torch.from_numpy(npy_f)

                        # The following are related to optical_flow map, Comment if not needed

                        # if frame <= 7:
                        #     # npy_next = np.load('/media' + batch_data[person][0][frame+1][5:] + '.npy').reshape(-1, 227, 227)
                        #     npy_next = io.imread('/media' + batch_data[person][0][frame + 1][5:] + '.jpg')
                        #     flow = get_flowmap.get_flow(npy, npy_next)
                        #     raw_x_flow[frame] = torch.from_numpy(flow.reshape(-1, 227, 227))
                        #
                        # if frame == 8:
                        #     # npy_next = np.load('/media' + next_batchdata[person][0][0][5:] + '.jpg').reshape(-1, 227, 227)
                        #     #
                        #     # flow = get_flowmap.get_flow(npy, npy_next)
                        #     raw_x_flow[frame] = raw_x_flow[frame - 1]
                        #     # print (raw_x.shape)
                    y = torch.from_numpy(np.array(batch_data[person][1])).long()

                    raw_x = Variable(raw_x.cuda()) if use_cuda else Variable(raw_x)
                    # raw_x_flow = torch.cat((raw_x_flow, padding), dim=1)
                    # raw_x_flow = Variable(raw_x_flow.cuda()) if use_cuda else Variable(raw_x_flow)
                    y = Variable(y.cuda()) if use_cuda else Variable(y)

                    x = model.cnn_model(raw_x)
                    x = x.view(n_steps, batch_size, -1)
                    # print (x.size())
                    # x_flow = model.cnn_model(raw_x_flow)
                    # x_flow = x_flow.view(n_steps, batch_size, -1)
                    # print (x_flow)

                    # x = torch.cat((x, x_flow), dim=-1)
                    try:
                        cb_loss, cb_tmp, cb_result, cb_ptft, topi = test_person(x, y, person_encoder,
                                                                batch_size, criterion)
                        # print('cb_ptft:', cb_ptft)
                        pft = torch.cat((cb_ptft, x), -1)
                        # print ('pft:', pft)

                        batch_ft[:, :, :, person] = pft
                    except RuntimeError as e:
                        print (e.message)
                # print (batch_ft[:, 0, :, :].size())
                ppooling = nn.MaxPool1d(n_person, stride=1).cuda()
                input_glstm = ppooling(batch_ft[:, 0, :, :])
                # print ('input_glstm:', input_glstm)
                input_glstm = input_glstm.view(n_steps, batch_size, -1)
                # print ('input:', input_glstm)
                # print ('target:', z)

                try:
                    cbg_loss, cb_gtmp = test_group(input_glstm, z, group_encoder,
                                                    group_encoder_hidden, criterion)
                    batch_loss = cbg_loss
                    batch_correct = cb_gtmp
                    # print (batch_correct)

                except RuntimeError as e:
                    print ('input_glstm.size', input_glstm.size())
                    print ('batch_ft[:, 0, :, :].size', batch_ft[:, 0, :, :].size())
                    print ('pft.size', pft.size())
                    print (e.message)

                final_loss = (batch_loss / (batch_size * n_steps)).data[0]
                final_acc = (100 * batch_correct) / (batch_size * n_steps)

                # print ('final loss:', final_loss)
                # print ('final acc:', final_acc)

                all_loss += final_loss
                all_acc += final_acc

                print ('Test_G:%s %d/%d--->epoch: %d---> batch_loss: %.4f---> batch_acc: %.2f %%'
                       % (pkl, idx, n_batch, n_iter, final_loss, final_acc))
    print ({'n_iters': n_iters,
                'all_batch': all_batch,
                'all_loss': all_loss / (all_batch * n_iters),
                'all_acc': all_acc / (all_batch * n_iters)})
    torch.save({'n_iters': n_iters,
                'all_batch': all_batch,
                'all_loss': all_loss / (all_batch * n_iters),
                'all_acc': all_acc / (all_batch * n_iters)},
               '/media/ld/dataset/new_cad/save_result/g_result/%s_gresult.tar'
               % model_name)


def gtest_main(person_encoder, saved_gmodel):
    hidden_dim_1 = 3000
    hidden_dim_2 = 1024

    group_encoder = model.test_LSTM(7096, hidden_dim_2, 7)
    # if optical_flow map involved, use this
    # encoder = model.test_LSTM(7096 + 4096, hidden_dim_1, 3).cuda()
    group_encoder = group_encoder.cuda()
    checkpoint = torch.load(saved_gmodel)
    group_encoder.load_state_dict(checkpoint['state_dict'])

    # print (person_encoder)
    # print (group_encoder)
    utils.okb('Person & Group encoder have been loaded!')

    all_data = utils.data_split()
    model_name = saved_gmodel[45:-11]

    testIters_Group(model_name, all_data[0][1], person_encoder=person_encoder, group_encoder=group_encoder)
    utils.okb('Training has been accomplished!')

# hidden_dim_1 = 3000
# person_encoder = model.test_LSTM(4096*2, hidden_dim_1, 3)
# person_encoder = person_encoder.cuda()
# checkpoint = torch.load('/media/ld/dataset/new_cad/save_model/p_model/Wed_Jan_31_06:55:48_2018_14_pmodel.tar')
# utils.okb('Person_encoder acc:')
# print (checkpoint['all_acc'])
# person_encoder.load_state_dict(checkpoint['state_dict'])
#
# gtest_main(person_encoder,
#            saved_gmodel='/media/ld/dataset/new_cad/save_model/g_model/Fri_Feb_02_04:24:37_2018_4_gmodel.tar')
