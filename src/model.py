# encoding: utf-8


import torch
import torchvision
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import utils
from torchvision.models import resnet34
import torch.utils.model_zoo as model_zoo

use_gpu = True

__all__ = ['AlexNet', 'alexnet']


model_urls = {
    'alexnet': 'https://download.pytorch.org/models/alexnet-owt-4df8aa71.pth',
}


class AlexNet(nn.Module):

    def __init__(self, num_classes=1000):
        super(AlexNet, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(64, 192, kernel_size=5, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
            nn.Conv2d(192, 384, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),
        )
        self.classifier = nn.Sequential(
            nn.Dropout(),
            nn.Linear(256 * 6 * 6, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
        )
        # self.fetr_extract = nn.Sequential(
        #     nn.Linear(256 * 6 * 6, 4096),
        #     nn.ReLU(inplace=True)
        # )

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), 256 * 6 * 6)
        x = self.classifier(x)
        return x

def alexnet(pretrained=False, **kwargs):
    r"""AlexNet model architecture from the
    `"One weird trick..." <https://arxiv.org/abs/1404.5997>`_ paper.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    model = AlexNet(**kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls['alexnet']))
    return model

# cnn_model = resnet34(pretrained=True).cuda() if use_gpu else resnet34(pretrained=True)
alexnet_inuse = AlexNet()
alexnet_inuse.load_state_dict(torch.load('/media/ld/dataset/new_cad/save_model/alexnet_feature.pth'))
cnn_model = alexnet_inuse.cuda()

class test_LSTM(nn.Module):

    def __init__(self, input_dim, hidden_dim, output_dim, n_layers=1):
        super(test_LSTM, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.n_layers = n_layers
        self.lstm = nn.LSTM(input_dim, hidden_dim, n_layers)
        self.out = nn.Linear(hidden_dim, output_dim)

    def forward(self, person_feature, hidden):
        person_feature = person_feature.view(9, 1, -1)
        output = person_feature
        # print (output.data.shape)

        for i in range(self.n_layers):
            output, hidden = self.lstm(output, hidden)
        classify_output = self.out(output)

        return classify_output, hidden, output

    def initHidden(self, batch_size):
        # print (batch_size)
        h_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim).cuda())
        c_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim).cuda())

        hc = (h_0, c_0)
        return hc

class LSTM_Person_encoder(nn.Module):

    def __init__(self, input_dim, hidden_dim, n_layers=1):
        super(LSTM_Person_encoder, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.lstm = nn.LSTM(input_dim, hidden_dim, n_layers)

    def forward(self, person_feature, hidden):
        person_feature = person_feature.view(1, -1, 512)
        output = person_feature
        print (output.data.shape)

        for i in range(self.n_layers):
            output, hidden = self.lstm(output, hidden)

        return output, hidden

    def initHidden(self, batch_size):
        h_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()
        c_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()

        hc = (h_0, c_0)
        return hc

# temp = utils.cnnf_extract('/home/ld/dataset/volleyball/0/')
# test_seq = temp[0][0]
#
# rnn = LSTM_Person_encoder(512, 256, 1).cuda()
# h_test = Variable(torch.randn(1, 12, 256)).cuda()
# c_test = Variable(torch.randn(1, 12, 256)).cuda()
# # hc = (h_test, c_test)
# hc = rnn.initHidden(12)
# lstm_out = []
# for ei in range(test_seq.size()[0]):
#     temp_fetr, _ = rnn(test_seq[ei], hc)
#     lstm_out[ei] = temp_fetr
# # print (temp_fetr[-1].size())
# print (len(lstm_out))

class LSTM_Group(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, n_layers=1):
        super(LSTM_Group, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.output_dim = output_dim
        self.n_layers = n_layers


        self.lstm = nn.LSTM(input_dim, hidden_dim, n_layers)
        self.out = nn.Linear(hidden_dim, output_dim)
        self.softmax = nn.Softmax()

    def forward(self, batch_feature, hidden):

        output = batch_feature
        # pmaxpooling = nn.MaxPool1d(n_person, stride=1)
        # output = pmaxpooling(output)
        # output = output.view(9, 1, -1)

        for i in range(self.n_layers):
            output = F.sigmoid(output)
            output, hidden = self.lstm(output, hidden)
        classify_output = self.out(output)

        return classify_output, hidden

    def initHidden(self, batch_size):
        h_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()
        c_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()

        hc = (h_0, c_0)
        return hc

class LSTM_Person_decoder(nn.Module):

    def __init__(self, hidden_dim, output_dim, n_layers=1, seq_len=10):
        super(LSTM_Person_decoder, self).__init__()
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers

        self.lstm = nn.LSTM(hidden_dim, hidden_dim, n_layers)
        self.out = nn.Linear(hidden_dim, output_dim)
        self.softmax = nn.LogSoftmax()

    def forward(self, person_feature, hidden):
        person_feature = person_feature.view(1, 1, -1)
        output = person_feature
        for i in range(self.n_layers):
            output = F.sigmoid(output)
            output, hidden = self.lstm(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

    def initHidden(self, batch_size):
        h_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()
        c_0 = Variable(torch.randn(self.n_layers, batch_size, self.hidden_dim)).cuda()

        return (h_0, c_0)


