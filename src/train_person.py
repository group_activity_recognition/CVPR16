# encoding: utf-8


from __future__ import unicode_literals, print_function, division
import torch
import torchvision
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F


import time
import random
import math
import os


from collections import defaultdict

import numpy as np
import data_processing
import model
import utils
import pickle
import test_person
from test_person import test_person
from test_person import testIters_p
from test_person import ptest_main
# import get_flowmap
import skimage.io as io

torch.cuda.set_device(1)

use_cuda = True
SOV_token = 0
EOV_token = 1

all_pkls = os.listdir('/media/ld/dataset/new_cad/new_scratch/')
test_pkls = [all_pkls[1]]

def asMinutes(s):
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)
def timeSince(since, percent):
    now = time.time()
    s = now - since
    es = s / (percent)
    rs = es - s
    return '%s (- %s)' % (asMinutes(s), asMinutes(rs))

base_path = '/media/ld/dataset/new_cad/new_scratch/'


def train_person(input_variable, target_variable, encoder, batch_size, encoder_optimizer,
                 criterion, n_steps=9):

    encoder_hidden = encoder.initHidden(batch_size)
    encoder_optimizer.zero_grad()


    # encoder_outputs dim : (9, 1, 3)
    encoder_outputs, encoder_hidden, p_tfetr = encoder(input_variable, encoder_hidden)

    pres = encoder_outputs.view(n_steps, 3)
    topv, topi = encoder_outputs.data.topk(1)

    result = torch.zeros(3, 4)
    tmp = 0
    for i, item in enumerate(topi):
        tmp += (item == target_variable.data[i]).cpu().numpy()[0][0]
        pre_id = item.cpu().numpy()[0][0]

        real_id = target_variable.cpu().data[i]
        if real_id == 0:
            result[0][0] += 1
            if pre_id == 0:
                result[0][1] += 1
            if pre_id == 1:
                result[0][2] += 1
            if pre_id == 2:
                result[0][3] += 1
        if real_id == 1:
            result[1][0] += 1
            if pre_id == 0:
                result[1][1] += 1
            if pre_id == 1:
                result[1][2] += 1
            if pre_id == 2:
                result[1][3] += 1
        if real_id == 2:
            result[2][0] += 1
            if pre_id == 0:
                result[2][1] += 1
            if pre_id == 1:
                result[2][2] += 1
            if pre_id == 2:
                result[2][3] += 1

    loss = criterion(pres, target_variable)

    loss.backward()
    encoder_optimizer.step()

    return loss, tmp, result, p_tfetr

def trainIters_p(training_pkls, encoder, n_iters, print_every=10, plot_every=10, batch_size=1, n_steps=9):
    start = time.time()
    # print (training_pkls)

    encoder_optimizer = optim.Adam(encoder.parameters())
    criterion = nn.CrossEntropyLoss()

    all_acc = 0
    all_loss = 0

    all_result = torch.zeros(3, 4)
    padding = torch.zeros(n_steps, 1, 227, 227)
    for n_iter in range(n_iters):
        utils.okb('Current iter: %d' % n_iter)
        all_batch = 0

        for pkl in training_pkls:
            # utils.ok('Result--------' * 5)
            # utils.ok('|\n'*10)
            # print (all_result)
            # utils.ok('|\n'*10)
            # utils.ok('Result--------' * 5)
            # seq_id = int(pkl.split('_')[1][3:])
            n_batch, tmp = utils.batch_newcad(base_path + pkl, batch_size)
            all_batch += n_batch
            utils.ok('Current pkl: %s,   Containing batches: %d' % (pkl, n_batch))
            for idx in range(n_batch):
                batch_data = tmp[idx: idx + batch_size][0]
                # next_batchdata = tmp[idx+1: idx + 1 + batch_size][0]

                n_person = len(batch_data)
                batch_loss = 0.0
                batch_correct = 0
                # print ('load batch_data')
                # print (n_person)

                for person in range(n_person):
                    raw_x = torch.zeros(n_steps, 3, 227, 227)
                    raw_x_flow = torch.zeros(n_steps, 2, 227, 227)

                    move_code = torch.zeros(n_steps, 1)

                    for frame in range(n_steps):
                        # print (batch_data[person][0][frame])
                        # print ('/media' + batch_data[person][0][frame][5:] + '.npy')
                        # npy = np.load('/media' + batch_data[person][0][frame][5:] + '.npy').reshape(-1, 227, 227)
                        npy = io.imread('/media' + batch_data[person][0][frame][5:] + '.jpg')
                        # print ('npy', npy)
                        npy_f = npy.reshape(-1, 227, 227).astype('float32')
                        # print ('npy_F', npy_f)
                        raw_x[frame] = torch.from_numpy(npy_f)

                        # The following are related to optical_flow map, Comment if not needed

                        # if frame <= 7:
                        #     # npy_next = np.load('/media' + batch_data[person][0][frame+1][5:] + '.npy').reshape(-1, 227, 227)
                        #     npy_next = io.imread('/media' + batch_data[person][0][frame + 1][5:] + '.jpg')
                        #     flow = get_flowmap.get_flow(npy, npy_next)
                        #     raw_x_flow[frame] = torch.from_numpy(flow.reshape(-1, 227, 227))
                        #
                        # if frame == 8:
                        #     # npy_next = np.load('/media' + next_batchdata[person][0][0][5:] + '.jpg').reshape(-1, 227, 227)
                        #     #
                        #     # flow = get_flowmap.get_flow(npy, npy_next)
                        #     raw_x_flow[frame] = raw_x_flow[frame-1]
                        # frame_num = int(batch_data[person][0][frame][51:].split('_')[0])
                        # person_bbox[frame][0], person_bbox[frame][1] = utils.get_bbox(seq_id, frame_num, person)
                        # # Calculate the move_code for each time step
                        # if frame <= 6:
                        #     move_code[frame] = utils.move_encoder(seq_id, frame_num, frame+1, frame_num+2, person)
                        # else:
                        #     move_code[frame] = move_code[frame-1]

                    y = torch.from_numpy(np.array(batch_data[person][1])).long()

                    balance = y < torch.zeros(y.size()).long()
                    assert torch.norm(balance.float(), 2) == 0, (y, idx, pkl)

                    raw_x = Variable(raw_x.cuda()) if use_cuda else Variable(raw_x)
                    # raw_x_flow = torch.cat((raw_x_flow, padding), dim=1)
                    # raw_x_flow = Variable(raw_x_flow.cuda()) if use_cuda else Variable(raw_x_flow)
                    y = Variable(y.cuda()) if use_cuda else Variable(y)

                    # print (raw_x.size())
                    # print (model.cnn_model)
                    x = model.cnn_model(raw_x)
                    x = x.view(n_steps, batch_size, -1)

                    # x_flow = model.cnn_model(raw_x_flow)
                    # x_flow = x_flow.view(n_steps, batch_size, -1)
                    # # print (x_flow)
                    #
                    # x = torch.cat((x, x_flow), dim=-1)
                    try:
                        cb_loss, cb_tmp, cb_result, cb_ptf = train_person(x, y, encoder, batch_size,
                                                               encoder_optimizer, criterion)
                        batch_loss += cb_loss
                        batch_correct += cb_tmp

                        all_result += cb_result
                    except RuntimeError as e:
                        print (e.message)
                # print(batch_loss)
                temp_loss = batch_loss.data.cpu().numpy()[0]

                final_loss = temp_loss / batch_size
                final_acc = (100 * batch_correct / (n_person * n_steps)) / batch_size
                # print (final_loss)
                # print (final_acc)
                all_loss += final_loss
                all_acc += final_acc
                print ('Training_P: %s %d/%d---->epoch: %d----> batch_loss: %.4f----> batch_acc: %.2f %%'
                       % (pkl, idx, n_batch, n_iter, final_loss, final_acc))

        if (n_iter+1) % 1 == 0:
        # if n_iter in [0, 2, 4]:
            save_path = '/media/ld/dataset/new_cad/save_model/p_model/%s_%d_pmodel.tar' \
                    % (time.strftime("%a_%b_%d_%H:%M:%S_%Y", time.localtime()), n_iter)
            torch.save({'n_iter': n_iter,
                        'state_dict': encoder.state_dict(),
                        'all_batch': all_batch,
                        'all_loss': all_loss / (all_batch * (n_iter + 1)),
                        'all_acc': all_acc / (all_batch * (n_iter + 1)),
                        'all_result': all_result.int()},
                        save_path)
            ptest_main(save_path)


hidden_dim_1 = 3000
encoder = model.test_LSTM(4096, hidden_dim_1, 3).cuda()
# if optical_flow map involved, use this
# encoder = model.test_LSTM(4096 * 2, hidden_dim_1, 3).cuda()
all_data = utils.data_split()
if 'split0_vid24_im.pkl' in all_data[0][0]:
    utils.okb('Find 24!')
print (all_data[0][0])

trainIters_p(all_data[0][0], encoder, n_iters=20)

