# encoding: utf-8


import os
import glob
import uuid
import scipy.misc
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from data_processing import get_video_annotations, annomat


def visualize(img_dir, anno):
    imfiles = sorted(glob.glob(os.path.join(img_dir, '*.jpg')))
    vidanno = get_video_annotations(anno)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(len(imfiles) // 2 + 3, len(imfiles), 3):
        im = scipy.misc.imread(imfiles[i])
        ax.imshow(im)
        for j in range(len(vidanno[i].tid)):
            idx = np.mod(vidanno[i].tid[j] * 10, 64)
            ax.add_patch(patches.Rectangle(vidanno[i].bbox[j][:2],
                                           vidanno[i].bbox[j][2],
                                           vidanno[i].bbox[j][3], linewidth=2, fill=False))
        plt.savefig('/media/ld/dataset/new_cad/{}.png'.format(uuid.uuid4()))
        plt.show()


if __name__ == '__main__':
    num = '01'
    anno = annomat('/media/ld/dataset/new_cad/annotations/anno{}.mat'.format(num))
    visualize('/media/ld/dataset/new_cad/images/seq{}'.format(num), anno)
