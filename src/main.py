# encoding: utf-8


import os
import torch
torch.cuda.set_device(2)

import model
import train_group
import train_person
import test_group
import utils



mode = {'train': True, 'test': False}
hidden_dim_1 = 3000
hidden_dim_2 = 500

person_encoder = model.test_LSTM(4096, hidden_dim_1, 3)
person_encoder = person_encoder.cuda()
checkpoint = torch.load('/media/ld/dataset/new_cad/save_model/p_model/Tue_Jan_16_03:52:48_2018_19_pmodel.tar')
print ('Person_encoder acc:', checkpoint['all_acc'])
person_encoder.load_state_dict(checkpoint['state_dict'])

if mode['train']:
    group_encoder = model.test_LSTM(7096, hidden_dim_2, 7)
    group_encoder = group_encoder.cuda()
    utils.okb('Person & Group encoder have been loaded!')
    # print (person_encoder)
    # print (group_encoder)

    all_data = utils.data_split()
    print ('training_pkls_1:', all_data[1][0])
    train_group.trainIters_Group(all_data[1][0], person_encoder=person_encoder, group_encoder=group_encoder)
    utils.okb('Training has been accomplished!')

if mode['test']:
    group_encoder = model.test_LSTM(7096, hidden_dim_2, 7)
    group_encoder = group_encoder.cuda()
    group_checkpoint = torch.load('/media/ld/dataset/new_cad/save_model/g_model/Tue_Jan_16_03:52:48_2018_19_pmodel.tar')
    group_encoder.load_state_dict(group_checkpoint['state_dict'])
    print ('Group_encoder acc:', group_checkpoint['all_acc'])
    utils.okb('Person & Group encoder have been loaded!')

    # print (person_encoder)
    # print (group_encoder)

    all_data = utils.data_split()
    print ('test_pkls_1:', all_data[0][1])
    test_group.testIters_Group(all_data[0][1], person_encoder=person_encoder, group_encoder=group_encoder)
    utils.okb('Training has been accomplished!')
